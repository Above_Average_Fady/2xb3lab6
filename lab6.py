class RBNode:

    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        self.parent = None
        self.colour = "R"

    def get_uncle(self):
        return

    def is_leaf(self):
        return self.left == None and self.right == None

    def is_left_child(self):
        return self == self.parent.left

    def is_right_child(self):
        return not self.is_left_child()

    def is_red(self):
        return self.colour == "R"

    def is_black(self):
        return not self.is_red()

    def make_black(self):
        self.colour = "B"

    def make_red(self):
        self.colour = "R"

    def get_brother(self):
        if self.parent.right == self:
            return self.parent.left
        return self.parent.right

    def get_uncle(self):
        return self.parent.get_brother()

    def uncle_is_black(self):
        if self.get_uncle() == None:
            return True
        return self.get_uncle().is_black()

    def __str__(self):
        return "(" + str(self.value) + "," + self.colour + ")"

    def __repr__(self):
         return "(" + str(self.value) + "," + self.colour + ")"

    def rotate_right(self):
        assert self.left.is_red()
        x = self.left
        self.left = x.right
        x.right = self
        x.colour = self.colour
        self.make_red()
        return x

    def rotate_left(self):
        assert self.right.is_red()
        if not(self.parent == None):
            x = self.right
            x.parent = self.parent
            self.parent.right = x
            self.parent = x
            self.right = None
            x.left = self
            x.colour = self.colour
            self.make_red()
        else:
            x = self.right
            self.right = x.left
            if not(x.left == None):
                x.left.parent = self
            x.left = self
            self.parent = x
            # x.colour = self.colour

        return x

    # def rotate_left(self):
    #     assert self.right.is_red()
    #     # x = RBNode(None)
    #     y = self.right
    #     self.right = y.left
    #     if not(y.left == None)
    #     self.parent.right = x
    #     self.parent = x
    #     self.right = None
    #     x.left = self
    #     x.colour = self.colour
    #     self.make_red()
    #     return x



class RBTree:

    def __init__(self):
        self.root = None

    def is_empty(self):
        return self.root == None

    def get_height(self):
        if self.is_empty():
            return 0
        return self.__get_height(self.root)

    def __get_height(self, node):
        if node == None:
            return 0
        return 1 + max(self.__get_height(node.left), self.__get_height(node.right))

    def insert(self, value):
        if self.is_empty():
            self.root = RBNode(value)
            self.root.make_black()
        else:
            self.__insert(self.root, value)

    def __insert(self, node, value):
        if value < node.value:
            if node.left == None:
                node.left = RBNode(value)
                node.left.parent = node
                self.fix(node.left)
            else:
                self.__insert(node.left, value)
        else:
            if node.right == None:
                node.right = RBNode(value)
                node.right.parent = node
                self.fix(node.right)
            else:
                self.__insert(node.right, value)

    def fix(self, node):
        #You may alter code in this method if you wish, it's merely a guide.
        if node.parent == None:
            node.make_black()
        while node != None and node.parent != None and node.parent.is_red() and node.is_red():
            print(T.__str__())
            if not(node.uncle_is_black()):
                unc = node.get_uncle()
                unc.make_black()
                node.parent.make_black()
                node.parent.parent.make_red()
                node = node.parent.parent
                print(node.value)
            else:
                if node.is_right_child() and node.parent.is_right_child():
                    print("OK")
                    print(node.parent.parent.value)
                    node.parent.parent.rotate_left()
                    print(node.value)
                    # node = node.parent

                elif node.is_left_child() and node.parent.is_left_child():
                    node.parent.parent.rotate_right()

        self.root.make_black()
                    
        
    def __str__(self):
        if self.is_empty():
            return "[]"
        return "[" + self.__str_helper(self.root) + "]"

    def __str_helper(self, node):
        if node.is_leaf():
            return "[" + str(node) + "]"
        if node.left == None:
            return "[" + str(node) + " -> " + self.__str_helper(node.right) + "]"
        if node.right == None:
            return "[" +  self.__str_helper(node.left) + " <- " + str(node) + "]"
        return "[" + self.__str_helper(node.left) + " <- " + str(node) + " -> " + self.__str_helper(node.right) + "]"

T = RBTree()
T.insert(3)
T.insert(1)
T.insert(6)
T.insert(5)
T.insert(7)
T.insert(8)
T.insert(9)
print("\n\n\n-------------------------------------------------------\n\n\n")
T.insert(10)
print("\n\n\n-------------------------------------------------------\n\n\n")
print(T.__str__())